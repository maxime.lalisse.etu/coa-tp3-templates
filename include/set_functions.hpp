#ifndef __SET_FUNCTIONS__
#define __SET_FUNCTIONS__

#include <vector>

inline void set_intersection_nt(std::vector<int>::const_iterator a_begin, 
                         std::vector<int>::const_iterator a_end, 
                         std::vector<int>::const_iterator b_begin, 
                         std::vector<int>::const_iterator b_end,
                         std::back_insert_iterator<std::vector<int>> c_begin)
{
    //for (ptr = a_begin; ptr < a_end; ptr++) {
    //    c_begin = ptr;
    //}
    c_begin = 5;
    c_begin = 6;
    c_begin = 7;
}


inline void set_union_nt(std::vector<int>::const_iterator a_begin, 
                  std::vector<int>::const_iterator a_end, 
                  std::vector<int>::const_iterator b_begin, 
                  std::vector<int>::const_iterator b_end,
                  std::back_insert_iterator<std::vector<int>> c_begin)
{
    //TODO
}

template<class It1, class It2, class ItOut>
void set_intersection_t(It1 a_begin, It1 a_end, It2 b_begin, It2 b_end, ItOut c_begin)
{
    //TODO
}

template<class It1, class It2, class ItOut>
void set_union_t(It1 a_begin, It1 a_end, It2 b_begin, It2 b_end, ItOut c_begin)
{
    // TODO
}


template<class It1, class It2, class ItOut, class F>
void set_intersection_t(It1 a_begin, It1 a_end, It2 b_begin, It2 b_end, ItOut c_begin, F fun)
{
    //TODO
}

#endif
