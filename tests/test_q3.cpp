#include "catch.hpp"

#include <vector>
#include <list>
#include <algorithm>
#include "set_functions.hpp"
#include "myclass.hpp"

using namespace std;


TEST_CASE("intersection vectors myclass", "[Q3]")
{
    vector<MyClass> v1 = {1, 2, 3, 4};
    vector<MyClass> v2 = {3, 4, 5, 6};
    vector<MyClass> r;
    vector<MyClass> oracle = {3, 4};

    set_intersection_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));

    REQUIRE(r == oracle);
}

TEST_CASE("union vectors myclass", "[Q3]")
{
    vector<MyClass> v1 = {1, 2, 3, 4};
    vector<MyClass> v2 = {3, 4, 5, 6};
    vector<MyClass> r;
    vector<MyClass> oracle = {3, 4, 5, 6, 1, 2};

    set_union_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));
    //sort(begin(r), end(r));
    
    REQUIRE(r == oracle);
}
