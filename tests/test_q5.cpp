#include "catch.hpp"

#include <vector>
#include <map>
#include <algorithm>
#include "set_functions.hpp"
#include "myclass.hpp"

using namespace std;

TEST_CASE("intersection map", "[Q5]")
{
    map<int, string> m1 = { {1, "a"}, {2, "b"}, {3, "c"} };
    map<int, string> m2 = { {2, "b"}, {3, "d"}, {4, "d"} }; // notez que {3, "d"} != {3, "c"}
    map<int, string> r;
    map<int, string> oracle = { {2, "b"} };
    
    set_intersection_t(begin(m1), end(m1), begin(m2), end(m2), inserter(r, r.end()));

    REQUIRE(r == oracle);
}
