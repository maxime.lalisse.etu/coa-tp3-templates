#include "catch.hpp"

#include <vector>
#include <list>
#include <algorithm>
#include "set_functions.hpp"

using namespace std;

TEST_CASE("intersection vectors", "[Q2]")
{
    vector<string> v1 = {"a", "b", "c", "d"};
    vector<string> v2 = {"c", "d", "e", "f"};
    vector<string> r;
    vector<string> oracle = {"c", "d"};

    set_intersection_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));

    REQUIRE(r == oracle);
}

TEST_CASE("union vectors", "[Q2]")
{
    vector<string> v1 = {"a", "b", "c", "d"};
    vector<string> v2 = {"c", "d", "e", "f"};
    vector<string> r;
    vector<string> oracle = {"a", "b", "c", "d", "e", "f"};

    set_union_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));
    sort(begin(r), end(r));
    
    REQUIRE(r == oracle);
}


TEST_CASE("intersection list", "[Q2]")
{
    list<string> v1 = {"a", "b", "c", "d"};
    list<string> v2 = {"c", "d", "e", "f"};
    list<string> r;
    list<string> oracle = {"c", "d"};

    set_intersection_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));

    REQUIRE(r == oracle);
}

TEST_CASE("union list", "[Q2]")
{
    list<string> v1 = {"a", "b", "c", "d"};
    list<string> v2 = {"c", "d", "e", "f"};
    list<string> r;
    list<string> oracle = {"c", "d", "e", "f", "a", "b"};

    set_union_t(begin(v1), end(v1), begin(v2), end(v2), back_inserter(r));
    //sort(begin(r), end(r)); // pas possible de faire quick sort sur une liste
    
    REQUIRE(r == oracle);
}

